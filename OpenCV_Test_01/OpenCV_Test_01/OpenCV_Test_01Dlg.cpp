
// OpenCV_Test_01Dlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "OpenCV_Test_01.h"
#include "OpenCV_Test_01Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
using namespace std;
using namespace cv;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// COpenCVTest01Dlg 대화 상자



COpenCVTest01Dlg::COpenCVTest01Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_OPENCV_TEST_01_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void COpenCVTest01Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(COpenCVTest01Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &COpenCVTest01Dlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// COpenCVTest01Dlg 메시지 처리기

BOOL COpenCVTest01Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void COpenCVTest01Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void COpenCVTest01Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR COpenCVTest01Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void COpenCVTest01Dlg::OnBnClickedButton1()
{
	IplImage * src = cvLoadImage("a.jpg", 0);

	//cvShowImage("src", src);
	//cvReleaseImage(&src);
	
	//cvWaitKey(0);
	//DisplayImage(IDC_PIC_2, src);
	
	Mat src1 = imread(string("a.jpg"));

	//circle(src1, cvPoint(50, 50), 20, cvScalar(255, 255, 255), 2);
	LARGE_INTEGER f, start, end;
	QueryPerformanceFrequency(&f);
	QueryPerformanceCounter(&start);

	//DisplayImage(IDC_PIC_1, src1(cvRect(0,0,src1.cols - src1.cols%4,src1.rows)).clone());
	DisplayImage(IDC_PIC_1, src1);
	//DisplayImage(IDC_PIC_2, src2);
	
	QueryPerformanceCounter(&end);
	double ms = (double)(end.QuadPart - start.QuadPart) / (f.QuadPart / 1000);
	ms++;

	cvReleaseImage(&src);
	src1.release();
	



}

void COpenCVTest01Dlg::DisplayImage(int IDC_PICTURE_TARGET, Mat targetMat)
{

	IplImage* targetIplImage = &IplImage(targetMat);// targetIplImage->widthStep++;
	if (targetIplImage != nullptr) {
		CWnd* pWnd_ImageTraget = GetDlgItem(IDC_PICTURE_TARGET);
		CClientDC dcImageTraget(pWnd_ImageTraget);
		RECT rcImageTraget;
		pWnd_ImageTraget->GetClientRect(&rcImageTraget);

		BITMAPINFO bitmapInfo;
		memset(&bitmapInfo, 0, sizeof(bitmapInfo));
		bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bitmapInfo.bmiHeader.biPlanes = 1;
		bitmapInfo.bmiHeader.biCompression = BI_RGB;
		bitmapInfo.bmiHeader.biWidth = targetIplImage->width;
		bitmapInfo.bmiHeader.biHeight = -targetIplImage->height;

		IplImage *tempImage = nullptr;
		tempImage = cvCreateImage(cvSize(targetIplImage->width, targetIplImage->height), IPL_DEPTH_8U, 3);
		if (targetIplImage->nChannels == 1)
		{
			cvCvtColor(targetIplImage, tempImage, CV_GRAY2BGR);
		}
		else if (targetIplImage->nChannels == 3)
		{
			LARGE_INTEGER f,start, end;
			QueryPerformanceFrequency(&f);
			QueryPerformanceCounter(&start);
			
//#pragma omp parallel for
			for (int i = 0; i < targetIplImage->height; i++)
			{
				memcpy(tempImage->imageData + i * tempImage->widthStep, targetIplImage->imageData + i * targetIplImage->widthStep, tempImage->widthStep);
			}
			
			QueryPerformanceCounter(&end);
			//3203*999 image = 4.32ms 
			//use openMP = 1.04ms
			double ms = (double)(end.QuadPart - start.QuadPart) / (f.QuadPart/1000);
			ms++;
		}

		bitmapInfo.bmiHeader.biBitCount = tempImage->depth * tempImage->nChannels;
		//dcImageTraget.SetStretchBltMode(HALFTONE);
		dcImageTraget.SetStretchBltMode(COLORONCOLOR);
		::StretchDIBits(dcImageTraget.GetSafeHdc(), rcImageTraget.left, rcImageTraget.top, rcImageTraget.right, rcImageTraget.bottom,
			0, 0, tempImage->width, tempImage->height, tempImage->imageData, &bitmapInfo, DIB_RGB_COLORS, SRCCOPY);

		cvReleaseImage(&tempImage);
	}



}
void COpenCVTest01Dlg::DisplayImage(int IDC_PICTURE_TARGET, IplImage *targetMat)
{
	if (targetMat != nullptr) {
		CWnd* pWnd_ImageTraget = GetDlgItem(IDC_PICTURE_TARGET);
		CClientDC dcImageTraget(pWnd_ImageTraget);
		RECT rcImageTraget;
		pWnd_ImageTraget->GetClientRect(&rcImageTraget);

		BITMAPINFO bitmapInfo;
		memset(&bitmapInfo, 0, sizeof(bitmapInfo));
		bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bitmapInfo.bmiHeader.biPlanes = 1;
		bitmapInfo.bmiHeader.biCompression = BI_RGB;
		bitmapInfo.bmiHeader.biWidth = targetMat->width;
		bitmapInfo.bmiHeader.biHeight = -targetMat->height;

		IplImage *tempImage = nullptr;
		tempImage = cvCreateImage(cvSize(targetMat->width, targetMat->height), IPL_DEPTH_8U, 3);
		if (targetMat->nChannels == 1)
		{
			LARGE_INTEGER f, start, end;
			QueryPerformanceFrequency(&f);
			QueryPerformanceCounter(&start);
			cvCvtColor(targetMat, tempImage, CV_GRAY2BGR);
			QueryPerformanceCounter(&end);

			double ms = (double)(end.QuadPart - start.QuadPart) / (f.QuadPart / 1000);
			ms++;
		}
		else if (targetMat->nChannels == 3)
		{
			LARGE_INTEGER f, start, end;
			QueryPerformanceFrequency(&f);
			QueryPerformanceCounter(&start);

			tempImage->imageData = targetMat->imageData;

			QueryPerformanceCounter(&end);
		
			double ms = (double)(end.QuadPart - start.QuadPart) / (f.QuadPart / 1000);
			ms++;
		}

		bitmapInfo.bmiHeader.biBitCount = tempImage->depth * tempImage->nChannels;
		//dcImageTraget.SetStretchBltMode(HALFTONE);
		dcImageTraget.SetStretchBltMode(COLORONCOLOR);
		::StretchDIBits(dcImageTraget.GetSafeHdc(), rcImageTraget.left, rcImageTraget.top, rcImageTraget.right, rcImageTraget.bottom,
			0, 0, tempImage->width, tempImage->height, tempImage->imageData, &bitmapInfo, DIB_RGB_COLORS, SRCCOPY);

		cvReleaseImage(&tempImage);
		//cvReleaseImage(&targetMat);
	}
}

